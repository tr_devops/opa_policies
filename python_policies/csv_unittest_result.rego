package main

default coverage = false

allow {
    coverage
}

deny {
    not allow
}

coverage {
    m := input.lines    
    m >= 35
}
