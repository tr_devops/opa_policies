package main

default coverage = false

allow {
    coverage
}

deny {
    not allow
}

coverage {
    m := input.totals.percent_covered    
    m >= 91
}
