package main

#default setVersion = false
#default build = false
#default tagRelease = false
#default promote = false

default allow = false

allow = true {
    count(check_include) == 4
}

#allow {
#  setVersion
#  build
#  tagRelease
#  promote
#}

#deny {
#   not allow
#}

check_include {
	m := input.include[i].file    
    m == "shared/setVersion.yml"
}

check_include {
	m := input.include[i].file    
    m == "shared/buildContainer.yml"
}

check_include {
	m := input.include[i].file    
    m == "shared/tagRelease.yml"
}

check_include {
	m := input.include[i].file    
    m == "shared/promoteContainer.yml"
}
