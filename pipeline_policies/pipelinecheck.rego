package pipelinecheck

default checkcx = false
default checksonar = false
default checkunit = false

checkcx {
	val := input[i]
	val.name == "CheckMarx"
}

checksonar {
	val := input[i]
	val.name == "Sonar"
}

checkunit {
	val := input[i]
	val.name == "Unit"
}
