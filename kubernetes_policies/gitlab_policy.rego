package main

default setVersion = false
default build = false
default tagRelease = false
default promote = false

allow {
  setVersion
  build
  tagRelease
  promote
}

deny {
   not allow
}

setVersion {
	m := input.include[i].file    
    m == "shared/setVersion.yml"
}

build {
	m := input.include[i].file    
    m == "shared/buildContainer.yml"
}

tagRelease {
	m := input.include[i].file    
    m == "shared/tagRelease.yml"
}

promote {
	m := input.include[i].file    
    m == "shared/promoteContainer.yml"
}
