package main

default evalType = false

allow {
    evalType
}

deny {
    not allow
}

evalType {
    m := input[i].type    
    m == "application"

}
